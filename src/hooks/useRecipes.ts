import { useState, useEffect, useCallback } from 'react';
import { useFetch } from './useFetch';
import { usePagination } from './usePagination';

import { RecipeProps, ApiResponse } from '../types';

export function useRecipes( requestUrl: string, recipesPerPage: number ){

    const { responseData, loading, setUrl } = useFetch<ApiResponse,any>();
    const { currentPage, totalPages, initPagin, incrPage, decrPage, resetPagin } = usePagination();
    const [ totalRecipes, setTotalRecipes ] = useState<RecipeProps[]>([]);
    const [ pageRecipes, setPageRecipes ] = useState<RecipeProps[]>([]);
  
    const requestQuery = useCallback( (query: string) => {
      setUrl( window.encodeURI( requestUrl + query ) );
    }, [requestUrl, setUrl]);
  
    useEffect(()=>{
      if( responseData ){
        setTotalRecipes(responseData.results);
      }
    }, [responseData]);


    useEffect(()=>{
      if(recipesPerPage && totalRecipes.length && currentPage){
        setPageRecipes( totalRecipes.slice( ( currentPage - 1 ) * recipesPerPage, currentPage * recipesPerPage ) );
      }
      else{
        setPageRecipes([]);
      }
    }, [recipesPerPage, totalRecipes, currentPage]);
    
    useEffect(()=>{
      if( recipesPerPage && totalRecipes.length ){
        initPagin( totalRecipes.length, recipesPerPage );
      }
      else{
        resetPagin();
      }
    }, [recipesPerPage, totalRecipes, initPagin, resetPagin ]);
  
    return { loading, pageRecipes, currentPage, totalPages, requestQuery, incrPage, decrPage, resetPagin };
}