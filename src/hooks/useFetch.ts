import { useState, useEffect } from 'react';
import 'whatwg-fetch';

export function useFetch<ResponseType,ErrorType>(){

    const [url, setUrl] = useState('');
    const [responseData, setResponseData] = useState<ResponseType|null>(null);
    const [error, setError] = useState<ErrorType|null>(null);
    const [loading, setLoading] = useState<boolean>(false);
  
    useEffect( () => {
      if(url){
        setLoading(true);
        window.fetch(url)
              .then(response=>response.json())
              .then(setResponseData)
              .catch(setError)
              .finally(()=> setLoading(false));
      }
    }, [url]);
  
    return { responseData, error, loading, setUrl };
}