import { useState, useCallback } from 'react';

export function usePagination(){
  
  const [currentPage, setCurrentPage] = useState<number>(0);
  const [totalPages, setTotalPages] = useState<number>(0);

  const initPagin = useCallback( ( itemsSize: number, itemsPerPage: number ) => {
    setCurrentPage(1);
    setTotalPages( Math.ceil( itemsSize / itemsPerPage ) );
  }, []);

  const incrPage = useCallback( () => {
    setCurrentPage(currentPage+1);
  }, [currentPage]);

  const decrPage = useCallback( () => {
    setCurrentPage(currentPage-1);
  }, [currentPage]);

  const resetPagin = useCallback( () => {
    setCurrentPage(0);
    setTotalPages(0);
  }, []);
  
  return { currentPage, totalPages, initPagin, incrPage, decrPage, resetPagin };
}
