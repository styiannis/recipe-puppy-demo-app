import { RecipeProps } from '../types';
import { RatioImage } from './RatioImage';
import './Recipe.css';

export const Recipe: React.FC<RecipeProps> = ({ href, title, thumbnail, ingredients }) => {
    return <div className="recipe">
                <a href={href} title={title} rel="noreferrer" target="_blank">
                    <RatioImage className="thumb" img={thumbnail} />
                    <span className="texts">
                        <span className="title">{title}</span>
                        <span className="ingredients">{ingredients}</span>
                    </span>
                </a>
            </div>;
}
