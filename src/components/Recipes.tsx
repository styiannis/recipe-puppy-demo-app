import { Recipe } from './Recipe';
import { RecipesProps } from '../types';

export const Recipes: React.FC<RecipesProps> = ({data}) => {
    return data.length ? 
        <div className="recipes">
            { data.map( (res,index) => 
                <Recipe
                    key={index}
                    href={res.href}
                    title={res.title}
                    thumbnail={res.thumbnail}
                    ingredients={res.ingredients} />
            ) }
        </div> : null;
}
