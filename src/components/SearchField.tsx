import { SearchFieldProps } from '../types';
import './SearchField.css';

export const SearchField: React.FC<SearchFieldProps> = ({ value = '', placeholder = 'Search recipe here...', onChange, onSubmit, clearField }) => {

    function onBlur() {}
    function onFocus() {}

    return <div className="search-field">
                <form action="." onSubmit={onSubmit} data-testid="form">
                    <input type="search" name="query" placeholder={placeholder} value={value} onChange={onChange} onFocus={onFocus} onBlur={onBlur}/>
                    <input type="submit" value="Submit" />
                </form>
                <button className="clear" onClick={clearField}>Clear</button>
            </div>;
}
