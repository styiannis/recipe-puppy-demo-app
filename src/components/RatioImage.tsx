import { useEffect, useState } from 'react';
import { RatioImageProps } from '../types';
import './RatioImage.css';

export const RatioImage: React.FC<RatioImageProps> = ({ img, ratio = 9 / 16, className }) => {

    const [styles, setStyles] = useState({});
    const [cname, setCname] = useState<string>('ratio-image');

    useEffect(() => {
        if( className ){
            setCname( 'ratio-image ' + className );
        }
    }, [className]);

    useEffect(() => {
        setStyles({
            backgroundImage: img ? 'url(' + img + ')' : null,
            paddingBottom: (100 * ratio) + '%',            
        });
    }, [img, ratio]);

    return <span className={cname}><span data-testid='ratio-image-inner' style={styles}></span></span>;
}
