import { PaginationProps } from '../types';
import './Pagination.css';

export const Pagination: React.FC<PaginationProps> = ({page, totalPages, nextPage, previousPage}) => {
    return 1 < totalPages ? <div className="pagination">
        { 1 < page ? <button className="previous" onClick={previousPage}>Previous</button> : null}
        { totalPages > page ? <button className="next" onClick={nextPage}>Next</button> : null }
    </div> : null;
}