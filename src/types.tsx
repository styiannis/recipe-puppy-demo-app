import { ChangeEvent, FormEvent, } from 'react';

export interface Recipe {
  href: string,
  title: string,
  thumbnail: string,
  ingredients: string,
}

export interface ApiResponse {
  title: string,
  version: number,
  href:  string,
  results: Recipe[],
}

/* ========== // Components prop types ========== */

export interface AppProps {
    requestBaseUrl: string,
    recipesPerPage: number,
}

export interface SearchFieldProps {
    value?: string,
    placeholder?: string,
    clearField: () => void,
    onChange: (ev: ChangeEvent<HTMLInputElement>) => void,
    onSubmit: (ev: FormEvent<HTMLFormElement>) => void,
}

export interface PaginationProps {
    page: number,
    totalPages: number,
    nextPage: () => void,
    previousPage: () => void,
}

export type RecipeProps = Recipe;

export interface RecipesProps {
    data: RecipeProps[],
}

export interface RatioImageProps {
    img: string,
    ratio?: number,
    className? : string,
}

/* ========== Components prop types // ========== */