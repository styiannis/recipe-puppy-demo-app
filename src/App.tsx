import { useState, useCallback, ChangeEvent, FormEvent } from 'react';
import { AppProps } from './types';
import { useRecipes } from './hooks';
import { Pagination, Recipes, SearchField } from './components';

import 'normalize.css';
import './App.css';

export const App: React.FC<AppProps> = ({ requestBaseUrl, recipesPerPage }) => {

  const [fieldValue, setFieldValue] = useState<string>('');

  const {
    loading,
    requestQuery,
    pageRecipes,
    currentPage,
    totalPages,
    incrPage,
    decrPage,
    resetPagin
  } = useRecipes( requestBaseUrl, recipesPerPage );

  const clearField = useCallback( ()=>{
    resetPagin();
    setFieldValue('');
  }, [resetPagin, setFieldValue]);

  const onChange = useCallback( (ev: ChangeEvent<HTMLInputElement>) => {
    setFieldValue(ev.target.value);
  }, [setFieldValue]);

  const onSubmit = useCallback( (ev: FormEvent<HTMLFormElement>) => {

    ev.preventDefault();
    ev.stopPropagation();

    const query = fieldValue.trim();

    if(query){
      requestQuery(query);
    }
    else{
      clearField();
    }

  }, [fieldValue, requestQuery, clearField]);
  
  return (
    <div className="App">
      <SearchField value={fieldValue} onChange={onChange} onSubmit={onSubmit} clearField={clearField} />
      { loading ? 'Loading...' : [
        <Pagination key='pagination' page={currentPage} totalPages={totalPages} nextPage={incrPage} previousPage={decrPage} />,
        <Recipes key='recipes' data={pageRecipes} /> ]
      }      
    </div>
  );
}

export default App;
