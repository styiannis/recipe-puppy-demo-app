import { renderHook, act } from '@testing-library/react-hooks'

import { usePagination } from '../../hooks';

const hookExports = ['currentPage', 'totalPages', 'initPagin', 'incrPage', 'decrPage', 'resetPagin'].sort();

const itemsSize = 33;
const itemsPerPage = 3;

test('pagination hook', async () => {

    const { result } = renderHook(() => usePagination());

    expect(Object.keys(result.current).sort()).toStrictEqual(hookExports);

    expect(result.current.currentPage).toBe(0);
    expect(result.current.totalPages).toBe(0);
    expect(typeof result.current.initPagin).toBe('function');
    expect(typeof result.current.incrPage).toBe('function');
    expect(typeof result.current.decrPage).toBe('function');
    expect(typeof result.current.resetPagin).toBe('function');

    act(() => {
        result.current.initPagin(itemsSize, itemsPerPage);
    });

    expect(result.current.currentPage).toBe(1);
    expect(result.current.totalPages).toBe(Math.ceil(itemsSize/itemsPerPage));

    act(() => {
        result.current.incrPage();
    });

    expect(result.current.currentPage).toBe(2);
    expect(result.current.totalPages).toBe(Math.ceil(itemsSize/itemsPerPage));

    act(() => {
        result.current.decrPage();
    });

    expect(result.current.currentPage).toBe(1);
    expect(result.current.totalPages).toBe(Math.ceil(itemsSize/itemsPerPage));

    act(() => {
        result.current.resetPagin();
    });

    expect(result.current.currentPage).toBe(0);
    expect(result.current.totalPages).toBe(0);
});
