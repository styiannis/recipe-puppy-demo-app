import { renderHook, act } from '@testing-library/react-hooks'
import { useFetch } from '../../hooks';

const hookExports = ['responseData', 'error', 'loading', 'setUrl'].sort();

const fetchUrl = 'api_fake_url';

const responseData = {
    key1: "value 1",
    key2: "value 2",
    key3: "value 3",
};

const errorResponse = 'error response';

afterEach(() => {
    window.fetch.mockClear();
});
  
afterAll(() => {
    window.fetch.mockRestore();
});

test('fetch success', async () => {

    jest.spyOn(window, "fetch").mockImplementation(() =>
        Promise.resolve({
            json: () => Promise.resolve(responseData),
        })
    );
    
    const { result, waitForNextUpdate } = renderHook(() => useFetch());

    expect(Object.keys(result.current).sort()).toStrictEqual(hookExports);

    expect(result.current.responseData).toBe(null);
    expect(result.current.error).toBe(null);
    expect(result.current.loading).toBe(false);
    expect(typeof result.current.setUrl).toBe('function');

    act(() => {
        result.current.setUrl(fetchUrl);
    });

    expect(result.current.loading).toBe(true);

    await waitForNextUpdate();

    expect(result.current.error).toBe(null);
    expect(result.current.loading).toBe(false);
    expect(result.current.responseData).toStrictEqual(responseData);
});

test('fetch error', async () => {

    jest.spyOn(window, "fetch").mockImplementation(() =>
        Promise.resolve({
            json: () => Promise.reject(errorResponse),
        })
    );

    const { result, waitForNextUpdate } = renderHook(() => useFetch());

    act(() => {
        result.current.setUrl(fetchUrl);
    });

    expect(result.current.loading).toBe(true);

    await waitForNextUpdate();

    expect(result.current.error).toBe(errorResponse);
    expect(result.current.loading).toBe(false);
    expect(result.current.responseData).toBe(null);
});