import { renderHook, act } from '@testing-library/react-hooks'

import { useRecipes } from '../../hooks';

const hookExports = [ 'loading', 'currentPage', 'totalPages', 'pageRecipes', 'requestQuery', 'incrPage', 'decrPage', 'resetPagin' ].sort();

const responseData = {
    title: "data title",
    version: 3.1,
    href: "data href",
    results: [
    {
        title: "result title 1",
        href: "result href 1",
        ingredients: "result ingredients 1",
        thumbnail: "result thumbnail 1",
    },
    {
        title: "result title 2",
        href: "result href 2",
        ingredients: "result ingredients 2",
        thumbnail: "result thumbnail 2",
    },
    {
        title: "result title 3",
        href: "result href 3",
        ingredients: "result ingredients 3",
        thumbnail: "result thumbnail 3",
    }]
};

const requestUrl = 'request_url';
const recipesPerPage = 2;

afterEach(() => {
    window.fetch.mockClear();
});
  
afterAll(() => {
    window.fetch.mockRestore();
});

test('recipes hook', async () => {

    jest.spyOn(window, "fetch").mockImplementation(() =>
        Promise.resolve({
            json: () => Promise.resolve(responseData),
        })
    );

    const { result, waitForNextUpdate } = renderHook(() => useRecipes( requestUrl, recipesPerPage ));

    expect(Object.keys(result.current).sort()).toStrictEqual(hookExports);

    expect(result.current.loading).toBe(false);
    expect(result.current.currentPage).toBe(0);
    expect(result.current.totalPages).toBe(0);
    expect(result.current.pageRecipes).toStrictEqual([]);
    expect(typeof result.current.requestQuery).toBe('function');
    expect(typeof result.current.incrPage).toBe('function');
    expect(typeof result.current.decrPage).toBe('function');
    expect(typeof result.current.resetPagin).toBe('function');

    act(() => {
        result.current.requestQuery('');
    });

    expect(result.current.loading).toBe(true);

    await waitForNextUpdate();

    expect(result.current.loading).toBe(false);
    expect(result.current.currentPage).toBe(1);
    expect(result.current.totalPages).toBe(2);
    expect(result.current.pageRecipes).toStrictEqual([responseData.results[0], responseData.results[1]]);

    act(() => {
        result.current.incrPage();
    });

    expect(result.current.currentPage).toBe(2);
    expect(result.current.totalPages).toBe(2);
    expect(result.current.pageRecipes).toStrictEqual([responseData.results[2]]);

    act(() => {
        result.current.decrPage();
    });

    expect(result.current.currentPage).toBe(1);
    expect(result.current.totalPages).toBe(2);
    expect(result.current.pageRecipes).toStrictEqual([responseData.results[0], responseData.results[1]]);

    act(() => {
        result.current.resetPagin();
    });

    expect(result.current.currentPage).toBe(0);
    expect(result.current.totalPages).toBe(0);
    expect(result.current.pageRecipes).toStrictEqual([]);
});
