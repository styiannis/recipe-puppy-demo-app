import { render, screen, fireEvent } from '@testing-library/react';
import { Pagination } from '../../components';

const propsAll = {
    page: 2,
    totalPages: 9,
    nextPage: jest.fn(),
    previousPage: jest.fn(),
};

const propsNoPagin = {
    ...propsAll,
    totalPages: 0,
};

const propsFirstPage = {
    ...propsAll,
    page: 1,
    totalPages: 2,
};

const propsLastPage = {
    ...propsAll,
    page: 2,
    totalPages: 2,
};

test('complete pagination', async () => {
    
    const { container } = render(<Pagination {...propsAll} />);
    
    expect(container.firstChild).toHaveClass('pagination', {exact: true});

    const previousButton = await screen.findByText('Previous');
    fireEvent.click(previousButton);
    expect(propsAll.previousPage).toHaveBeenCalled();

    const nextButton = await screen.findByText('Next');
    fireEvent.click(nextButton);
    expect(propsAll.nextPage).toHaveBeenCalled();
});

test('no pagination', () => {
    const { container } = render(<Pagination {...propsNoPagin} />);
    expect(container.firstChild).toBe(null);
});

test('first page pagination', () => {
    
    render(<Pagination {...propsFirstPage} />);
    
    const previousButton = screen.queryByText('Previous');
    expect(previousButton).toBe(null); 

    const nextButton = screen.queryByText('Next');
    expect(nextButton).toBeInTheDocument();
});

test('last page pagination', () => {
    
    render(<Pagination {...propsLastPage} />);

    const previousButton = screen.queryByText('Previous');
    expect(previousButton).toBeInTheDocument();   
    
    const nextButton = screen.queryByText('Next');
    expect(nextButton).toBe(null);  
});
