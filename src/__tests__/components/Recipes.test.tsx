import { render } from '@testing-library/react';
import { Recipes } from '../../components';

const propsAll = {
    data: [
        {
            href: 'recipe href 1',
            title: 'recipe title 1',
            thumbnail: 'recipe thumb src 1',
            ingredients: 'recipe ingredient a 1, recipe ingredient b 1',
        },
        {
            href: 'recipe href 2',
            title: 'recipe title 2',
            thumbnail: 'recipe thumb src 2',
            ingredients: 'recipe ingredient a 2, recipe ingredient b 2',
        }
    ]
};

const propsEmptyData = {
    data: [],
};

test('recipes', () => {
    const { container } = render(<Recipes {...propsAll} />);
    expect(container.firstChild).toHaveClass('recipes', {exact: true});
});

test('empty recipes array', () => {
    const { container } = render(<Recipes {...propsEmptyData} />);
    expect(container.firstChild).toBe(null);
});
