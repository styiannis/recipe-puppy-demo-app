import { render } from '@testing-library/react';
import { Recipe } from '../../components';

const propsAll = {
    href: 'recipe href',
    title: 'recipe title',
    thumbnail: 'recipe thumb src',
    ingredients: 'recipe ingredient 1, recipe ingredient 2',
};

test('complete pagination', () => {
    const { container } = render(<Recipe {...propsAll} />);
    expect(container.firstChild).toHaveClass('recipe', {exact: true});
});
