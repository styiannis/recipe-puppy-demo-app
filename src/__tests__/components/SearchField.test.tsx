import { render, screen, fireEvent } from '@testing-library/react';
import { SearchField } from '../../components';

const defaultInputValue = '';
const defaultInputPlaceholder = 'Search recipe here...';

const newInputValue = 'new search query';

const propsMandatory = {
    onChange: jest.fn(),
    onSubmit: jest.fn(),
    clearField: jest.fn(),
};

const propsAll = {
    ...propsMandatory,
    value: 'initial value',
    placeholder: 'custom placeholder',
};

test('search field', async () => {
    
    const { container, rerender } = render(<SearchField {...propsMandatory} />);

    expect(container.firstChild).toHaveClass('search-field', {exact: true});

    let input;

    input = screen.getByPlaceholderText(defaultInputPlaceholder);
    expect(input).toBeInTheDocument();
    expect(input).toHaveAttribute('value', defaultInputValue);

    rerender(<SearchField {...propsAll} />);

    input = screen.getByPlaceholderText(propsAll.placeholder);
    expect(input).toBeInTheDocument();
    expect(input).toHaveAttribute('value', propsAll.value);

    fireEvent.focus(input);
    fireEvent.change(input, { target: { value: newInputValue } });
    fireEvent.blur(input);
    expect(propsAll.onChange).toHaveBeenCalled();

    const clearButton = await screen.findByText('Clear');
    fireEvent.click(clearButton);
    expect(propsAll.clearField).toHaveBeenCalled();

    const form = screen.getByTestId('form');
    fireEvent.submit(form);
    expect(propsAll.onSubmit).toHaveBeenCalled();
});
