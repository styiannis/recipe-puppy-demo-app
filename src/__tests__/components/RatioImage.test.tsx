import { render } from '@testing-library/react';
import { RatioImage } from '../../components';

const propsAll = { 
    img: 'ratio-image-src',
    ratio: 50,
    className: 'ratio-image-container',
};

test('ratio image', () => {

    const { container, getByTestId } = render(<RatioImage {...propsAll} />);
    
    expect(container.firstChild).toHaveClass('ratio-image ' + propsAll.className, {exact: true});
    
    const ratioImageInner = getByTestId('ratio-image-inner');
    expect(container.firstChild).toContainElement(ratioImageInner);

    expect(ratioImageInner).toHaveStyle({ 
        paddingBottom: (100 * propsAll.ratio) + '%',
        backgroundImage: propsAll.img
    });
});

test('ratio image without image source', () => {
    
    const { container, getByTestId } = render(<RatioImage img='' />);
    
    expect(container.firstChild).toHaveClass('ratio-image', {exact: true});

    const ratioImageInner = getByTestId('ratio-image-inner');
    expect(ratioImageInner).toHaveStyle({ 
        paddingBottom: (100 * (9/16)) + '%'
    });
});
