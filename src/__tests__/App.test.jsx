import { render, screen, fireEvent } from '@testing-library/react';
import { App } from '../App';

const propsAll = {
    requestBaseUrl: '',
    recipesPerPage: 10,
};

const defaultInputPlaceholder = 'Search recipe here...';

test('App component', () => {

    const { container } = render(<App {...propsAll} />);
    
    expect(container.firstChild).toHaveClass('App', {exact: true});
    
    const searchFieldForm = screen.getByTestId('form');
    const searchFieldInput = screen.getByPlaceholderText(defaultInputPlaceholder);

    expect(searchFieldInput.value).toBe('');
    fireEvent.submit(searchFieldForm);

    fireEvent.change(searchFieldInput, { target: { value: 'abc' } });    
    expect(searchFieldInput.value).toBe('abc');
    fireEvent.submit(searchFieldForm);
});