# Recipe Puppy demo app

> This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm run proxy-server`

Launches a proxy server used as middleware on API requests to avoid CORS restrictions.\
Ιt is necessary to execute this command before executing one of the following scripts ( on a different CLI ):

- npm start
- npm run serve

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`

Builds the app for production to the `build` folder.

### `npm run serve`

Serves files from the `build` folder.\
Open [http://localhost:5000](http://localhost:5000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run test:coverage`

Launches the test runner and code coverage statistics in the interactive watch mode.
